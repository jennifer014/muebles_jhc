import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import { AboutAs, CartProvider, Item, ItemListContainer, NavBar, Offers, Shop } from './components';
import { useContext, useState } from 'react';
import { Toast, ToastContainer } from 'react-bootstrap';
import { MessageContext } from "./components/Contexts/MessageContext";

function App() {

  const [position, setPosition] = useState('bottom-end');
  //const [showA, setShowA] = useState(true);
  const { message, setShowA, showA } = useContext(MessageContext);
  const close = () => {
    setShowA(false);
}
  return (
   
      <CartProvider>
        <ToastContainer position={position}>
          <Toast show={showA} onClose={() => close(false)} delay={3000} autohide bg={message.bg}>
                <Toast.Header>
                  <img src="holder.js/20x20?text=%20" className="rounded me-2" alt=""/>
                  <strong className="me-auto">{message && message.title}</strong>
                </Toast.Header>
                <Toast.Body className={'text-white'}>{message && message.description}</Toast.Body>
              </Toast>
        </ToastContainer>
        <BrowserRouter>
        <div className="App">
            <NavBar/>
            <div>
            </div>
        </div>
        <Routes>
        <Route path='' element={<ItemListContainer greeting="FUNKO - POP | Todos los productos"/>}></Route>
          <Route path='/offers' element={<Offers/>}></Route>
          <Route path='/Category/:id' element={<ItemListContainer greeting="FUNKO - POP | Filtros"/>}></Route>
          <Route path='/AboutAs' element={<AboutAs/>}></Route>
          <Route path='/Item/:id' element={<Item />}></Route>
          <Route path='/Shop' element={<Shop/>}></Route>
        </Routes>
        </BrowserRouter>
      </CartProvider>
  );
}

export default App;
