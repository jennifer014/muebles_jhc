import {getFirestore, doc, getDoc, collection, getDocs, query, where, addDoc} from 'firebase/firestore'

const findDataFb = async () => {
  const db = getFirestore();
  const funkoCollection = collection(db, 'items')
  const snap = await getDocs(funkoCollection);

  return snap.docs.map(doc => ({id: doc.id, ...doc.data()}));
};


const findByIdFb = async (id) => {
  const db = getFirestore();
  const element = doc(db, "items", id);
  const snap = await getDoc(element);
  console.log(snap.data());
  return {id: snap.id, ...snap.data()}
};

const findByCategoryFb = async (category) => {
  const db = getFirestore();
  const funkoCollection = query(collection(db, 'items'), where("categoryId", "==", category) )
  const snap = await getDocs(funkoCollection);

  return snap.docs.map(doc => ({id: doc.id, ...doc.data()}));
};

const saveDataFb = async (shop) => {
  const db = getFirestore();
  const funkoCollection = collection(db, 'shop');
  const snap = await addDoc(funkoCollection, shop);

  return {id: snap.id}
};


export const funkoService = { findDataFb, findByIdFb, findByCategoryFb, saveDataFb};