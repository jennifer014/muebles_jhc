import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { MessageProvider } from './components';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB40C_T8gbyN1dthHoU4UcoTHp1JldaDqE",
  authDomain: "jhcsolutions-eb6b8.firebaseapp.com",
  projectId: "jhcsolutions-eb6b8",
  storageBucket: "jhcsolutions-eb6b8.appspot.com",
  messagingSenderId: "973812561119",
  appId: "1:973812561119:web:f6f076afecaaf8dad1256a",
  measurementId: "G-5DREK7D9JK"
};

// Initialize Firebase
initializeApp(firebaseConfig);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
     <MessageProvider>
    <App />
    </MessageProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
