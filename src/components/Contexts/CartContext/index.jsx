import { useState } from 'react';
import {createContext} from 'react';

const CartContext = createContext({})

const CartProvider = ({children}) => {

    const [cart, setCart] = useState([]);
    const [total, setTotal] = useState(0);

    const deleteItemCart = (item) => {
        console.log(item);
        const newCart = [...cart];
        let totalDeleted = 0;
        const index = newCart.find((detail) => detail.id === item.id);
        if(index){
            totalDeleted = index.quantity * item.price;
            setTotal(total-totalDeleted);
            const productsNotDeleted = newCart.filter((detail) => detail.id !== item.id);
            setCart(productsNotDeleted);
        }
        
    };

    const addToCart = (item, quantity) => {
        const cartLocal = [...cart];
        let totalLocal = 0;
        const index = cartLocal.find((detail) => detail.id === item.id);
        if(index){
            index.quantity = index.quantity + quantity;
            totalLocal += index.quantity * item.price;
            setCart(cartLocal);
        } else {
            item.quantity = quantity;
            totalLocal += quantity * item.price;
            setCart(prevArray => [...prevArray, item]);
        }
        setTotal(totalLocal);
      }

    return(
        <CartContext.Provider value={{ cart, addToCart, total, setCart, deleteItemCart }}>
            {children}
        </CartContext.Provider>
    )
}

export { CartProvider, CartContext }