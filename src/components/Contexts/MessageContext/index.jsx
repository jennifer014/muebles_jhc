import { useState } from 'react';
import {createContext} from 'react';

const MessageContext = createContext({})

const MessageProvider = ({children}) => {

    const [showA, setShowA] = useState(false);
    const [message, setMessage] = useState({});

    const pushMessage = (data) => {
        setShowA(true);
        setMessage(data);
    }

    return(
        <MessageContext.Provider value={{ pushMessage, message, showA, setShowA }}>
            {children}
        </MessageContext.Provider>
    )
}

export { MessageProvider, MessageContext }