import { useContext, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { CartContext} from "../Contexts/CartContext";
import { FaArrowLeft, FaTrash, FaLongArrowAltRight } from "react-icons/fa";
import { useForm } from "react-hook-form";
import './index.css';
import { ErrorField } from "../ErrorField";
import { MessageContext } from '../Contexts/MessageContext';
import { funkoService } from "../../services/funko";
import { useNavigate } from 'react-router-dom';
import { LinkContainer } from "react-router-bootstrap";

export const Shop = () =>{
    const [inputs, setInputs] = useState({});
    const [costShipping, seCostShipping] = useState(20);
    const navigate = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();//https://react-hook-form.com/get-started
    const { pushMessage } = useContext(MessageContext);

    const { deleteItemCart, cart, total, setCart } = useContext(CartContext);

    

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}))
      }

    const onSubmit = (e) => {
        const data = {
            'client': inputs,
            'items': cart,
            'total': total+costShipping
        };
        funkoService.saveDataFb(data).then((data) => {
            console.log(data.id)
            pushMessage({'title': 'Éxito', 'description': 'Su compra ha sido guardada con Nº de pedido: '+data.id, bg: 'success' });
            navigate("/");
            setCart([]);
        });

    }
    return (
        <>  
        {cart.length === 0 ?(
                <Row>
                    <Col>
                        <div className="card" style={{textAlign:'center'}}>
                            <div className="container">
                                <h2>CARRITO</h2>
                                <p className="title">Carrito vacio</p>
                                <img src="https://xn--latiendariberea-crb.com/principal/assets/img/carrito_vacio.png" alt=""/>
                                <LinkContainer to="/"><p><button className="button"><FaArrowLeft/>Regresar al comercio</button></p></LinkContainer>
                            </div>
                        </div>
                    </Col>
                </Row>) : 
            <form onSubmit={handleSubmit(onSubmit)}>
                <Container className="mt-5 p-3 rounded cart">
                    <Row className="no-gutters">
                        <Col className="col-md-8">
                            <div className="product-details mr-2">
                                <div className="d-flex flex-row align-items-center">
                                    <LinkContainer to="/"><FaArrowLeft/></LinkContainer>
                                    <span style={{marginLeft:15}}>Continuar comprando</span>
                                </div>
                                <h6 className="mb-0">Carrito de compras</h6>
                                <div className="d-flex justify-content-between"><span>Usted tiene {cart.length} productos en su carrito</span>
                                </div>
                                    {cart.map((funko) => {
                                        return (
                                            <Row key={funko.id}>
                                                <div className="d-flex justify-content-between align-items-center p-2 items rounded" style={{marginTop:15}} >
                                                    <div className="d-flex flex-row"><img className="rounded" src={funko.image} width="40" alt=""/>
                                                    <div style={{marginLeft:15}}>
                                                        <span className="d-block" style={{fontWeight: 'bold'}}>{funko.title}</span>
                                                        <span className="spec">{funko.description}</span></div>
                                                    </div>
                                                    <div className="d-flex flex-row align-items-center">
                                                        <span className="d-block">{funko.quantity}</span>
                                                        <span className="d-block" style={{fontWeight: 'bold', marginLeft:100}}>${funko.price}</span>
                                                    <FaTrash onClick={() => deleteItemCart(funko)}/>
                                                    </div>
                                                </div>
                                            </Row>
                                        );
                                    })}
                                    
                                <hr/>
                            </div>
                        </Col>
                        <Col>
                            <div className="payment-info">
                                <div className="d-flex justify-content-between align-items-center">
                                    <span>Detalles de tarjeta</span>
                                    <img className="rounded" src="https://i.imgur.com/WU501C8.jpg" width="30" alt=""/>
                                </div>
                                <span className="type d-block mt-3 mb-1">Escoja la tarjeta</span>
                                <label className="radio"> 
                                    <input type="radio" name="card" value="mastercard" onChange={handleChange}
                                    {...register("card", { required: true })}/> 
                                    <span><img width="30" src="https://img.icons8.com/color/48/000000/mastercard.png" alt=""/></span> 
                                </label>
                                <label className="radio"> 
                                    <input type="radio" name="card" value="visa" onChange={handleChange}
                                    {...register("card", { required: true })}/> 
                                    <span><img width="30" src="https://img.icons8.com/officel/48/000000/visa.png" alt=""/></span> 
                                </label>
                                <label className="radio">
                                    <input type="radio" name="card" value="amex" onChange={handleChange}
                                    {...register("card", { required: true })}/> 
                                    <span><img width="30" src="https://img.icons8.com/ultraviolet/48/000000/amex.png" alt=""/></span> 
                                </label>
                                <label className="radio"> 
                                    <input type="radio" name="card" value="paypal" onChange={handleChange}
                                    {...register("card", { required: true })}/>
                                    <span><img width="30" src="https://img.icons8.com/officel/48/000000/paypal.png" alt=""/></span> 
                                </label>
                                {errors.card && <ErrorField field="Tarjeta"/>}
                                <div>
                                    <label className="credit-card-label">Nombre y Apellido</label>{errors.username && <ErrorField field="Nombre y Apellido"/>}
                                    <input type="text" className="form-control credit-inputs" placeholder="Nombre"
                                    {...register("username", { required: true })}
                                    name="username"
                                    value={inputs.username || ""} 
                                    onChange={handleChange}/>
                                    
                                </div>
                                <div>
                                    <label className="credit-card-label">Numero de la tarjeta</label>{errors.card_number && <ErrorField field="Numero de la tarjeta"/>}
                                    <input type="text" className="form-control credit-inputs" placeholder="0000 0000 0000 0000"
                                    {...register("card_number", { required: true })}
                                    name="card_number"
                                    value={inputs.card_number || ""} 
                                    onChange={handleChange}/>
                                    
                                </div>
                                <Row>
                                    <Col>
                                        <label className="credit-card-label">Fecha</label>{errors.date && <ErrorField field="Fecha"/>}
                                        <input type="text" className="form-control credit-inputs" placeholder="12/24"
                                        {...register("date", { required: true })}
                                        name="date"
                                        value={inputs.date || ""} 
                                        onChange={handleChange}/>
                                        </Col>
                                    <Col>
                                        <label className="credit-card-label">CVV</label>{errors.cvv && <ErrorField field="CVV"/>}
                                        <input type="text" className="form-control credit-inputs" placeholder="342"
                                        {...register("cvv", { required: true })}
                                        name="cvv"
                                        value={inputs.cvv || ""} 
                                        onChange={handleChange}/>
                                    </Col>
                                </Row>
                                <div>
                                    <label className="credit-card-label">Dirección</label>{errors.address && <ErrorField field="Dirección"/>}
                                    <input type="text" className="form-control credit-inputs" placeholder="Madrid, España"
                                    {...register("address", { required: true })}
                                    name="address"
                                    value={inputs.address || ""} 
                                    onChange={handleChange}/>
                                </div>
                                <div>
                                    <label className="credit-card-label">Email</label>
                                    {errors.email?.type === 'required' && <ErrorField field="Email"/>}
                                    {errors.email?.type === 'pattern' && <span className="text-danger" style={{fontSize:10}}>El Campo Email no comple con el formato</span>}
                                    <input type="text" className="form-control credit-inputs" placeholder="usuario@gmail.com"
                                    {...register("email", { required: true, pattern:/^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/})}
                                    name="email"
                                    value={inputs.email || ""} 
                                    onChange={handleChange}/>
                                </div>
                                <div>
                                    <label className="credit-card-label">Teléfono</label>{errors.phone && <ErrorField field="Teléfono"/>}
                                    <input type="text" className="form-control credit-inputs" placeholder="5939991999121"
                                    {...register("phone", { required: true })}
                                    name="phone"
                                    value={inputs.phone || ""} 
                                    onChange={handleChange}/>
                                    
                                </div>
                                <hr className="line"/>
                                <div className="d-flex justify-content-between information">
                                    <span>Subtotal</span><span>${total}</span>
                                </div>
                                <div className="d-flex justify-content-between information">
                                    <span>Envío</span><span>${costShipping}</span>
                                </div>
                                <div className="d-flex justify-content-between information">
                                    <span>Total(Incl. impuestos)</span><span>${total+costShipping}</span>
                                </div>
                                <button className="btn btn-primary d-flex justify-content-between mt-3" style={{width:'100%'}} type="submit">
                                    <span>${total+costShipping}</span>
                                    <span>Pagar<FaLongArrowAltRight/></span>
                                </button></div>
                        </Col>
                    </Row>
                </Container>
            </form>
        }
     </>
    );
}
