import Nav from 'react-bootstrap/Nav';
import {Badge} from "react-bootstrap";
import { FaShoppingCart } from "react-icons/fa";
import { LinkContainer } from 'react-router-bootstrap';
import { useContext } from 'react';
import { CartContext } from "../Contexts/CartContext";

export const CartWidget = () =>{

    const { cart } = useContext(CartContext)


    return (
        <LinkContainer to="/Shop"><Nav.Link href="#jhc">
            <span>
                <FaShoppingCart size={30}/>
                <Badge bg="danger" pill="true">{cart.length}</Badge>
            </span></Nav.Link></LinkContainer>
    );
}
