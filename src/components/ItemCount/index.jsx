import './style.css';

import { useState } from 'react';
import { Button, Row } from 'react-bootstrap';
import { CartContext} from "../Contexts/CartContext";
import { useContext } from 'react';
import { MessageContext } from '../Contexts/MessageContext';

export const ItemCount = ({id, stock, valueInitQuantity, item}) =>{

  const [quantity, setQuantity] = useState(valueInitQuantity);

  const countQuantity = (num) => {
    setQuantity(quantity + num);
  };

  const { addToCart, cart } = useContext(CartContext);
  const { pushMessage } = useContext(MessageContext);

  
  const handleChange = event => {
    event.preventDefault();
  }

  const addToShop = () => {
    let actual = quantity;
    let message = 'Producto(s) agregado al carrito';
    let isAvaliable=true;
    if(cart && cart.length){
      const findItem = cart.find((det) => det.id === item.id);
      if(findItem){
        actual = actual + findItem.quantity;
        message = 'Producto actualizado al carrito';
      }
    }

    if(actual > Number(stock)){
      isAvaliable = false;
    }

    if(isAvaliable){
     addToCart(item, quantity);
     pushMessage({'title': 'Éxito', 'description': message , bg: 'success' });
    } else {
      pushMessage({'title': 'Sin Stock Disponible!!!', 'description': 'Estimado usuario ya agrego todo el stock disponible!', bg: 'danger' });
    }
  };

  return (
    <>
        <div className="product-counter">
            <input id="minus" type="button" value="-" 
            onClick={() => countQuantity(-1)}
            disabled={quantity === valueInitQuantity ? true : null}/>
            <input id="quantity" type="text" value={quantity} name="quantity"  onChange={handleChange} />
            <input id="plus" type="button" value="+" onClick={() => countQuantity(+1)}
            disabled={quantity === stock ? true : null}/>
        </div>
        <Row>
            <Button variant="primary" onClick={() => addToShop()}>Agregar</Button>
        </Row>
    </>

  );
}
