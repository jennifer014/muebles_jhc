import { useState } from "react";
import { useEffect } from "react";
import { Card, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { funkoService } from "../../services/funko";
import { ItemCount } from "../ItemCount";

const Item = () => {
  const [item, setIem] = useState();

  const { id } = useParams();

  useEffect(() => {
    funkoService.findByIdFb(id).then((data) => setIem(data));
  }, [id]);

  return <div style={{'textAlign': '-webkit-center'}}>{item && <Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src={item.image} width={200} height={200}/>
  <Card.Body>
    <Row>
     <span style={{'float':'right', 'color': 'red', 'fontSize':10}}>Stock:<label>{item.stock}</label></span>
    </Row>
    <Card.Title children={item.name}/>
    <Card.Text children={item.description}/>
    <ItemCount stock={item.stock} valueInitQuantity={1} id={item.id} item={item}/>
  </Card.Body>
</Card>}</div>;
};

export { Item };