import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import logo from '../../assets/images/LogoFunko.png';
import { NavLink } from 'react-router-dom';
import { CartWidget } from '../CartWidget';
import { LinkContainer } from 'react-router-bootstrap';
export const NavBar = ({count}) =>{
    return (<Navbar bg="light" expand="lg" variant="light">
        <Container>
            <Navbar.Brand><NavLink  to="/"><img src={logo} alt="logo" style={{ width: 100, height: 50 }}/></NavLink></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                <LinkContainer to="/"><Nav.Link >Inicio</Nav.Link></LinkContainer>
                    <NavDropdown title="Categorías" id="basic-nav-dropdown">
                    <LinkContainer to={`/Category/Disney`}><NavDropdown.Item>Disney</NavDropdown.Item></LinkContainer>
                    <LinkContainer to={`/Category/Anime`}><NavDropdown.Item>Anime</NavDropdown.Item></LinkContainer>
                        <NavDropdown.Divider />
                        <LinkContainer to="/">
                            <NavDropdown.Item>
                            Ver todas
                        </NavDropdown.Item></LinkContainer>
                    </NavDropdown>
                    <NavDropdown title="Nosotros" id="basic-nav-dropdown">
                    <LinkContainer to="/AboutAs"><NavDropdown.Item>Quienes Somos</NavDropdown.Item></LinkContainer>
                        <NavDropdown.Item href="#mision">Misión</NavDropdown.Item>
                        <NavDropdown.Item href="#vision">Visión</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#contactUs">
                            Contáctanos
                        </NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                <Nav>
                    <CartWidget countItems={count}/>
                </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
    );
}

