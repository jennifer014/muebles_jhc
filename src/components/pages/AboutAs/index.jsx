import './style.css';

export const AboutAs = () =>{
    return (
        <>
        <div className="about-section">
            <h1>Quienes somos</h1>
            <p>Venta de Funko Pops</p>
            <p>@Copyright 2023</p>
        </div>

        <h2>Nuestro team</h2>
        <div className="row">
        <div className="column">
            <div className="card">
            <div className="container">
                <h2>Jennifer Honores</h2>
                <p className="title">CEO & Founder</p>
                <p>Diseño pagina Funko Pop.</p>
                <p>jhonores@coderhouse.com</p>
                <p><button className="button">Contact</button></p>
            </div>
            </div>
        </div>
        </div>
</>
    );
}
