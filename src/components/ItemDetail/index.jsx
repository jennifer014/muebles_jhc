import {  Row } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';

export const ItemDetail = ({item}) =>{
  return (
    <Card style={{ width: '18rem' }}>
      <LinkContainer to={`/item/${item.id}`}><Card.Img variant="top" src={item.image} width={200} height={200}/></LinkContainer>
      <Card.Body>
        <Row>
         <span style={{'float':'right', 'color': 'red', 'fontSize':10}}>Stock:<label>{item.stock}</label></span>
        </Row>
        <Card.Title children={item.title}/>
        <Card.Text children={item.description}/>
        <Link to={`/item/${item.id}`}><span style={{cursor:'pointer'}}>Ver producto</span></Link>
      </Card.Body>
    </Card>
  );
}
