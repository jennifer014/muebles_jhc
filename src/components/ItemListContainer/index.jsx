import './style.css';
import { funkoService } from "../../services/funko";
import { useEffect, useState } from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap';
import { ItemDetail } from '../ItemDetail';
import { useNavigate, useParams } from 'react-router-dom';

export const ItemListContainer = ({greeting}) =>{
    const [funkos, setFunkos] = useState([]);
    const { id } = useParams();
    const navigate = useNavigate();

      useEffect(() => {
        if(id){
            funkoService.findByCategoryFb(id).then((data) => setFunkos(data))
            .catch(
                (err) => {
                    alert(err);
                    navigate("/");
            }
            );
        }else {
            funkoService.findDataFb().then((data) => setFunkos(data));
        }
      }, [navigate, id]);

      console.log(funkos);


    return (
        <>
            <Card>
                <Card.Body className="header">{greeting} {id}</Card.Body>
            </Card>
            <Container>

            <Row>
                {funkos.map((funko) => {
            return (
                <Col style={{'padding': 20}} key={funko.id}>
                    <ItemDetail item={funko}/>
                </Col>
            );
            })}
            </Row>
            </Container>
     </>
    );
}
